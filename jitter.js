
var bulloterie
var sumExp
var treemap = d3.treemap();

function preload(){
  var bulloterie = loadJSON("bulloterie.json", gotData)
  createCanvas(windowWidth, windowHeight)
  noCanvas()
  
  var width = 350
  var height = 350




  return
}

function setup(){
 

 //console.log(bulloterie.expert.length)

}

var playerList = []

function newInscription(name) {
  
}

function gotData(data){
  
  //const margin = {top:40,right:40,bottom:40,left:40}

  var force ;

  // test using d3-hierarchy
  data = d3.hierarchy({children: data}, function(d) {
    console.log("children")
    if (d.children)
      return d.children
    if (d["type"] == "person")
      return undefined

    return ["expert", "passionné", "intéressé"].flatMap((level) =>
      d[level].map((id) => ({"type":"person", "level": level, "name": id, "value": 1}))
    )
  })
    .count(d => d.size)

  console.log(data)

  

  

  var clusterLayout = d3.pack()
  .size([width - 2, height - 2])
  const root = d3.pack()
  .size([width - 2, height - 2])
  .padding(10)
  (data)



  var nodes = clusterLayout(root
    .sum(function(d) { return d.value; })
    .sort(function(a, b) { return b.height - a.height || b.value - a.value; }))
    .descendants()
  
 
  // var links = data.links()
  


  console.log(nodes)



  var base = d3.select("#cont")

  
  var chart = base.append("canvas")
    .attr("width", width)
    .attr("height", height)

  var context = chart.node().getContext("2d"),
  w = width,
  h = height;
  
  d3.selectAll("canvas")
    .call(d3.drag()
        .container(chart.node())
        .subject(dragsubject)
        .on("start", dragstarted)
        .on("drag", dragged)
        .on("end", dragended))
    .call(d3.zoom())
        
  var canvas = base.append("custom")

  var element = base.selectAll("custom.g")
  .data(root.descendants())
  .enter().append("custom.g")
  .attr("transform", d => `translate(${d.x + 1},${d.y + 1})`)
  .attr("width", w)
  .attr("height", h)

  // var bulle = element.filter(d => d.data.type != "person" && d.depth > 1)
  // var person = element.filter(d => d.data.type == "person")
  // var expert = element.filter(d => d.data.type == "person" && d.data.level == "expert")
  // var passionne = element.filter(d => d.data.type == "person" && d.data.level == "passionné")
  // var interesse = element.filter(d => d.data.type == "person" && d.data.level == "intéressé")
  
  

 var simulation = d3.forceSimulation(nodes)
    .force("charge", d3.forceManyBody(nodes))
    // .force("link", d3.forceLink(links).distance(10).strength(0.5))
    .force("x", d3.forceX())
    .force("y", d3.forceY())
    .force("collide", d3.forceCollide(10))
    .on("tick", ticked);

  

  function ticked() {
    context.clearRect(0, 0, w, h);
    context.save();
    context.translate(w / 2, h / 2);
    
    
    context.beginPath();
    // links.forEach(drawLink);
    context.strokeStyle = "#bbb";
    context.stroke();
   
    context.beginPath();
    nodes.forEach(drawNode);
    
    context.restore();

    
    
      
  }
  
  function dragsubject() {
    return simulation.find(d3.event.x - w / 2, d3.event.y - h / 2);
  }
  
  function dragstarted() {
    if (!d3.event.active) simulation.alphaTarget(0.3).restart();
    d3.event.subject.fx = d3.event.subject.x;
    d3.event.subject.fy = d3.event.subject.y;
  }
  
  function dragged() {
    d3.event.subject.fx = d3.event.x;
    d3.event.subject.fy = d3.event.y;
  }
  
  function dragended() {
    if (!d3.event.active) simulation.alphaTarget(0);
    d3.event.subject.fx = null;
    d3.event.subject.fy = null;
  }
  
  function drawLink(d) {
    context.moveTo(d.source.x, d.source.y);
    context.lineTo(d.target.x, d.target.y);
  }
  // qsdf
  function drawNode(d) {
    context.moveTo(d.x + d.r/3, d.y);
    
    if(d.data){
    if(d.data.type != "person" && d.depth >= 1){
    context.fillText( d.data.nom, d.x, d.y);
    context.arc(d.x, d.y, d.r/3, 0, 2 * Math.PI);
    context.strokeStyle = "black";
    context.fillStyle = "black";

    }else if(d.data.type == "person" && d.data.level == "expert" ){
     
      //context.fill();
      context.fillStyle = "white"
      context.rect(d.x-d.r, d.y, d.r*2,d.r*2);
     

      context.textBaseline = "hanging";
      context.textAlign="center"; 
      //context.fill();
      context.fillStyle = "red"
      context.fillText( d.data.name, d.x, d.y);

      
      
    }else if(d.data.type == "person" && d.data.level == "passionné" ){
      
      context.strokeStyle = "blue";
      context.fillStyle = "null"
      //context.fill();
      //context.stroke();
      context.arc(d.x, d.y, d.r, 0, 2 * Math.PI);

      context.textBaseline = "hanging";
      context.textAlign="center"; 
      
      context.fillStyle = "blue"
      //context.fill();
      context.fillText( d.data.name, d.x, d.y);

    }}
  }

  // bulle.append("circle")
  // .attr("x", d => d.x)
  //   .attr("y", d => d.y)
  // .attr("r", d => d.r)
  // .attr("fill", d => d3.scaleSequential(d3.interpolateMagma).domain([8, 0])(d.height))
  // //.append().text(d => d.data.name)

  // passionne.append("circle")

  //   .attr("r", d => d.r)
  //   .attr("fill", d => d3.scaleSequential(d3.interpolateMagma).domain([8, 0])(d.height));



  expert.append("rect")
    .attr("transform", d => `translate(${-d.r},${-d.r})`)
    .attr("width", d => d.r * 1.7)
    .attr("height", d => d.r * 1.7)
    .attr("fill", d => d3.scaleSequential(d3.interpolateMagma).domain([8, 0])(d.height));
  

  person.append("text")
  .attr("font-family", "serif")
  .attr("font-size", "12px")
  .attr("text-anchor", "middle")
  .style("fill","black")
  .text(d => d.data.name)


  bulle.append("text")
    .attr("font-family", "serif")
    .attr("font-size", "12px")
    .attr("text-anchor", "middle")
    .style("fill","black")
    .text(d => d.data.nom)



   // .style("writing-mode", "tb") // set the writing mode

return;
  
  // var x = d3.scale.linear()
  //                     .domain([0,50])
  //                     .range([0,width -margin.left - margin.right])
  // var y = d3.scale.linear()
  //                     .domain([0,50])
  //                     .range([height-margin.left - margin.right,0])


    
    var nomSvg = d3.selectAll("body")
    .data(data)
    .enter()
    .append("svg")
    .attr("width", 200)
    .attr("height", 250)
    .append("p")
    .text(function(d) { return d.nom })

    var txte = nomSvg.selectAll("g")
    .data(function(d){return d})
    .enter()
    .append("svg")
    .attr("width", 200)
    .attr("height", 40)
    .append("text")
    .attr("x",20)
    .attr("y", 10)
    .attr("width", function(d){return d.width})
    .attr("height", function(d){return d.height})
    .text(function(d,i){ return " " + d})
    //.attr("textLength", function(d){return d.width})
    .attr("font-family", "serif")
    .attr("font-size", "12px")
    .attr("text-anchor", "middle")
    .style("fill","black")
    .style("writing-mode", "tb") // set the writing mode
    .style("glyph-orientation-vertical", 0)
    .append("g")



    var expSvg = d3.selectAll("body")
    .data(data)
    .enter()
    .append("svg")
    .attr("x", function(d,i){return i*30})
    .attr("y", 0)
    .attr("width", 30)
    .attr("height", 30)
    .append("g")
    //.attr("transform","translate("+ margin.left +","+ margin.top +")")
  


  var rect = expSvg.selectAll("g")

    .data(function(d){return d.expert})
    .enter()

    .append("rect")
    .attr("x", function(d,i){return i*30})
    .attr("y", 0)
    .attr("width", 25)
    .attr("height", 25)
    .style("fill","red")
    .append("g")
                     
  
  var rectNum = rect.select("g")
    .data(function(d){return d.expert})
    .enter()
    .append("text")
    .attr("x", 12.5)
    .attr("y", function(d,i){return (i)*30+16.5})
    .text(function(d,i){
      return d})
    .attr("font-family", "serif")
    .attr("font-size", "14px")
    .attr("text-anchor", "middle")
    .style("fill","white")

  // var expSvg2 = expSvg.selectAll("svg")
  //                     .data( data)
  //                     .enter()

  // var expSvg3 = expSvg2.append("svg")
  // .attr("x", function(d){return d.width})
    
  // .attr("y", function(d,i){
  //   i= (i+1)*25 -12
  //   return i})
  // .attr("width", 25)
  // .attr("height", 25)                      

                    
  // var sqTxt = expSvg2.append("rect")
  //                   .attr("x",0)
  //                   .attr("y", 0)
  //                   .attr("width", 17)
  //                   .attr("height", 17)
  //                   .style("fill","red")
                    


  // var experts = expSvg3.select("text")
  //                   .data(data)
  //                   .enter(function(d){return d.expert})
  //                   .append("text")    

  // var optExperts = expSvg3.append("text")
  //                   .attr("x", 5)
  //                   .attr("y", 5)
  //                   .text(function(d,i){
  //                     return d.expert.length})
  //                   .attr("font-family", "arial")
  //                   .attr("font-size", "10px")
  //                   .style("fill","white")

  // console.log(d3.keys(function(d){return d}))
  
  

  function draw(){
    //background(255)


}
}
