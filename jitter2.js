var addBubble;
var addBubbleButton;
var canvas


var bubblesForm=[]
var bubbles = []
var players =[]
var slots= []
var pseudoList =[]
var playersForm = []
var levels = ["Interet, Passion, Expert"]
var inscriptions = []
var colors = ["aqua", "black", "blue", "fuchsia", "gray", "green", "lime", "maroon", "navy", "olive", "purple", "red", "silver", "teal", "white", "yellow"]



function setup(){
noCanvas()
background(255)

    for(i=0; i< 100; i++){
        slots.push(i)
    } 

    //Sets of players data to play with
    players = [
        {"pseudo" : "Ana","numero" : 10,"color" : d3.rgb(color(random(0,255),random(0,255),random(0,255))),"colorSel" : 0,"click" : 0,"selected" : false,"hover":false},
        {"pseudo" : "Pierre","numero" : 54,"color" : d3.rgb(color(random(0,255),random(0,255),random(0,255))),"colorSel" : 0,"click" : 0,"selected" : false,"hover":false},
        {"pseudo" : "Marie","numero" : 22,"color" : d3.rgb(color(random(0,255),random(0,255),random(0,255))),"colorSel" : 0,"click" : 0,"selected" : false,"hover":false},
        {"pseudo" : "Thalassa","numero" : 45,"color" : d3.rgb(color(random(0,255),random(0,255),random(0,255))),"colorSel" : 0,"click" : 0,"selected" : false,"hover":false},
        {"pseudo" : "Dié","numero" : 15,"color" : d3.rgb(color(random(0,255),random(0,255),random(0,255))),"colorSel" : 0,"click" : 0,"selected" : false,"hover":false},
        {"pseudo" : "Didier","numero" : 89,"color" : d3.rgb(color(random(0,255),random(0,255),random(0,255))),"colorSel" : 0,"click" : 0,"selected" : false,"hover":false},
        {"pseudo" : "Charlie","numero" : 89,"color" : d3.rgb(color(random(0,255),random(0,255),random(0,255))),"colorSel" : 0,"click" : 0,"selected" : false,"hover":false},
        {"pseudo" : "314r4te","numero" : 87,"color" : d3.rgb(color(random(0,255),random(0,255),random(0,255))),"colorSel" : 0,"click" : 0,"selected" : false,"hover":false},
        {"pseudo" : "Roxanne","numero" : 52,"color" : d3.rgb(color(random(0,255),random(0,255),random(0,255))),"colorSel" : 0,"click" : 0,"selected" : false,"hover":false},
        {"pseudo" : "Margot","numero" : 13,"color" : d3.rgb(color(random(0,255),random(0,255),random(0,255))),"colorSel" : 0,"click" : 0,"selected" : false,"hover":false},
        {"pseudo" : "Pierrot","numero" : 18,"color" : d3.rgb(color(random(0,255),random(0,255),random(0,255))),"colorSel" : 0,"click" : 0,"selected" : false,"hover":false},
        {"pseudo" : "Olivier","numero" : 68,"color" : d3.rgb(color(random(0,255),random(0,255),random(0,255))),"colorSel" : 0,"click" : 0,"selected" : false,"hover":false},
        {"pseudo" : "Bichette","numero" : 25,"color" : d3.rgb(color(random(0,255),random(0,255),random(0,255))),"colorSel" : 0,"click" : 0,"selected" : false,"hover":false},
    
    ]

    makeGrid() // display player grid 
}

//SortPlayers according to playergrid options
function sortPlayers(){
    players.sort(function(a,b){
        return d3.ascending(a.numero,b.numero)
    })
}

//Append a grid of svg card for all players
function makeGrid(){

    //Remove all element before redrawing
    var del = d3.selectAll("#playergrid").selectAll("g").remove()
   
    //Select the div and put data to display
    var grid = d3.selectAll("#playergrid").selectAll("g")
        .data(players)
        .enter()
        .append("div")
        .append("g")
    

    var colorS = grid
        .append("svg")
        .attr("width","20%")
        .attr("height","20%")
        .append("rect")
        .attr("position","relative")
        .attr("width","100%")
        .attr("height","100%")
        .attr("id", "colorselector") // < Gives id for later use
        .attr("fill",function(d){return d.color})
        .attr("x","100%")
        .attr("y","0%")
        .transition().delay(500).attr("x","0%") //< Keeps delay to synchro with other animation
        // .attr("fill", function(d){return d.color})
        .attr("z-index","1")
        .on("click", function(d){
            d.colorSel ++
            if((d.colorSel)%8 == 1) {d3.select(this).attr("fill", "Orange")}
            if((d.colorSel)%8 == 2) {d3.select(this).attr("fill", "Aqua")}
            if((d.colorSel)%8 == 3) {d3.select(this).attr("fill", "DeepPink")}
            if((d.colorSel)%8 == 4) {d3.select(this).attr("fill", "DarkOrange")}
            if((d.colorSel)%8 == 6) {d3.select(this).attr("fill", "LawnGreen")}
            if((d.colorSel)%8 == 0) {d3.select(this).attr("fill", d.color)}
    
            return(console.log("ok"))
        })
    //Create a svg card for each datum in players array

    var slot = grid
        .append("svg")
        .attr("id", "slotcard")
        .attr("class", "inp")
        .attr("box-sizing","border-box")
        .on("mouseenter", function(d){return console.log("in" + d.pseudo)})

 

        //Visualisation and animation when clicked svg card
        .on("click",function(d){
        
            //When clic and the svg card is default size
            if(!d.hover){
                //Select and make svg card bigger
                var k = d3.select(this)
                    .style("height","40%")
                    .style("width","85%")
                  
                    

                //Add a rectangle  
                d3.select(this).append("rect")
                    .attr("id", "infoplayer") // < Gives id for later use
                    .attr("width","75%")
                    .attr("height","70%")
                    .attr("x","-100%")
                    .attr("y","25%")
                    .transition().delay(500).attr("x","3%") //< Keeps delay to synchro with other animation
                    .attr("fill","white")
              
           

                //Player's pseudo attributes
                d3.select(this).select("#pseudoanchor").select("text")
                    .transition().attr("y","18.5%")
                    .transition().attr("x","3%")
                    .transition().attr("font-size", "325%")

                //Player's number attribute
                d3.select(this).select("#numberanchor").select("text")
                    .attr("text-anchor","end") //< Keeps text edge to the svd left border
                    .attr("x","5%")
                    .transition().attr("y","95%")
                    .transition().attr("font-size", "400%")              
                    .transition().attr("x","95%")
                
                //Keep the svg card opened
                d.hover = true

            //When click and svg card is bigger   
            }else{
                d3.select(this).select("#colorSelector").remove()
                //Player's pseudo attributes
                d3.select(this).select("#pseudoanchor").select("text")
                    .transition().attr("font-size", "110%")
                    .transition().attr("x","10%")
                    .transition().attr("y","75%")
                    
                //Player's number attributes
                d3.select(this).select("#numberanchor").select("text")
                .attr("text-anchor","start").attr("x","90%") //< Keeps text edge to the svd right border
                .transition().attr("font-size", "110%")
                .transition().attr("x","3%")
                .transition().attr("y","75%")
                
                //Resizes svg card to default
                d3.select(this)
                    .style("height","10%")
                    .style("width","95%")

                //Removes the infoplayer rect
                d3.select(this).selectAll("#infoplayer").remove()
                
                //Keeps the svg card closed
                d.hover = false
                
            }
        })
        //Visualisation and animation when double clicked on svg card
        .on("dblclick",function(d){
            d.click ++
            if(!d.selected){
                d.selected = true
                d3.select(this).style("border-right", "0px solid#000000").style("border-radius", "0px")
            }else{ 
                d.selected = false
                d3.select(this).style("border-right", "15px solid red").style("border-radius", "2px")
            }

            if((d.click)%4 == 0) {d3.select(this.firstChild).style("fill", "red")}
            if((d.click)%4 == 1) {d3.select(this.firstChild).style("fill", "green")}
            if((d.click)%4 == 2) {d3.select(this.firstChild).style("fill", "orange")}
            if((d.click)%4 == 3) {d3.select(this.firstChild).style("fill", d.color)}
            if(d.click >4 ){return d.click = 0}
        })
        //Visualisation and animation goes out of svg card
        .on("mouseout",function(d){
            // if(d.hover){
            // d3.select(this)
            // .style("height","15%")
            // .style("width","90%")
            // }
        })
    
    //Group all nexts elements inside svg card

 

    var slotcard = slot
        .append("g")

    //Fill all svg with a rect. Rect's color is configurable by click
    var cardback = slotcard
        .append("rect")  
        .attr("position","relative")
        .attr("width", "100%")
        .attr("height", "100%")
        .attr("fill", function(d,i){return d.color})

    var colorS = slotcard
      
        .append("rect")
        .attr("position","relative")
        .attr("width","100%")
        .attr("height","100%")
        .attr("id", "colorselector") // < Gives id for later use
        .attr("fill",function(d){return d.color})
        .attr("x","100%")
        .attr("y","0%")
        .transition().delay(500).attr("x","0%") //< Keeps delay to synchro with other animation
        // .attr("fill", function(d){return d.color})
        .attr("z-index","1")
        .on("click", function(d){
            d.colorSel ++
            if((d.colorSel)%8 == 1) {d3.select(this).attr("fill", "Orange")}
            if((d.colorSel)%8 == 2) {d3.select(this).attr("fill", "Aqua")}
            if((d.colorSel)%8 == 3) {d3.select(this).attr("fill", "DeepPink")}
            if((d.colorSel)%8 == 4) {d3.select(this).attr("fill", "DarkOrange")}
            if((d.colorSel)%8 == 6) {d3.select(this).attr("fill", "LawnGreen")}
            if((d.colorSel)%8 == 0) {d3.select(this).attr("fill", d.color)}
    
            return(console.log("ok"))
        })

            
// PlayerCard info display

        var numberanchor = slot
            .append("g")
            .attr("id","numberanchor")

        var pseudoanchor = slot
            .append("g")
            .attr("id","pseudoanchor")   

        var num = numberanchor
            .append("text")
            .transition()
            .text(function(d,i ){return d.numero})
            .attr("font-family", "sans-serif")
            .attr("fill", "white")
            .attr("font-weight", "bold")
            .attr("padding","2px 2px 2px 2px")
            //.attr("font-size-adjust", "0.5")
            .transition().attr("font-size", "0%")
            .transition().attr("font-size", "110%")
            .attr("x","3%")
            .attr("y","75%")

        var pseudo = pseudoanchor
            .append("text")
            .transition()
            .text(function(d){ return d.pseudo})
            .attr("font-family", "sans-serif")
            .attr("font-weight", "bold")
            .attr("fill","white")
            .transition().attr("font-size", "0%")
            .transition().attr("font-size", "110%")
            .transition().attr("x","-50%")
            .transition().attr("x","10%")
            .attr("y","75%")
            .on("mouseover", function(d){
                //d3.select(this).attr("font-size", "14px").attr("y", "95%")
            })
            .on("mouseout", function(d){
            //  d3.select(this).attr("font-size", "14px").attr("y", "60%")
            })

        
 

        
      

}

function gridData(){

    var data = new Array()
    var xpos = 1
    var ypos = 1
    var width = 10
    var height = 10
    var cells = 100
    var rows = 10
    var colomns = cells/rows
    var click = 0

    for(var row =0; row < rows; row++){
        data.push(new Array())

        for(var colomn =0; colomn<colomns;colomn++){
            data[row].push({
                x: xpos + "%",
                y: ypos + "%",
                width: width + "%",
                height: height + "%",
                click : click
            })
            xpos+= width
            
        }
        xpos = 1
        ypos += height
    }

    return data;
}

function inscriptionAdd(){

    inscriptions.push({
        "player" : playerNumber,
        "bubble" : bubbleName
    })
}


// Add a bubbles to the page
function bubbleAdd(){

    // Create an input to choose bubble's names
    if(bubblesForm != undefined)
    bubblesForm.push(new bubbleForm())

   
    for(i=0; i< bubblesForm.length;i++){
        if(i == bubblesForm.length-1){
        bubblesForm[i].display()  // Display all the bubbles 
        }
    }

}

//Add player when submit is correctly inputed
function playerAdd(){

    //Get html input a and b values
    a = document.getElementById("playerPseudo")
    aVal = a.value.toUpperCase()

    b = document.getElementById("playerNumber")
    bVal = b.value.toUpperCase()
    
    //Find pseudo value is already in pseudolist array
    c = pseudoList.findIndex(function(element){return element == aVal})
    d = slots.findIndex(function(element){return element == bVal})

    //The entered pseudo is available
    if(c == -1){

        //but empty... 
        //Clears html input and paints its right border with red
        if(!aVal){
            console.log("Player empty")
            d3.select(a)
            .attr("placeholder", "Pseudo manquant")
            .style("border-right-color" , "red")
            .style("border-right-width", "12px")
            .style("border-style","solid")
            a.value = ""  // < This clears input value to make placeholder update
   
        }else{ //Keeps Html input value and paints right border with green  
            d3.select(a)
            .style("border-right-color" , "green")
            .style("border-right-width", "12px")
            .style("border-style","solid")
            a.value = aVal
        }
        
    //The entered pseudo is already used 
    //Replace input placeholder and say "[speudo]* already takenl"
    }else{
        d3.select(a)
            .attr("placeholder", "Le pseudo "+aVal + " est déjà pris")
            .style("border-right-color" , "orange")
            .style("border-right-width", "12px")
            .style("border-style","solid")
        a.value = ""
    }

    //Find if number value is already in slots array
    if(d > -1){

        //but empty... 
        //Clears html input and paints its right border with red
        if(!bVal){
            console.log("Number empty")
            d3.select(b)
                .attr("placeholder", "Numéro manquant")
                .style("border-right-color" , "red")
                .style("border-right-width", "12px")
                .style("border-style","solid")
            b.value = ""
        
        
        }else{ //Keeps Html input value and paints right border with green  
            d3.select(b)
                .style("border-right-color" , "green")
                .style("border-right-width", "12px")
                .style("border-style","solid")
            b.value = bVal
        }
        
        
    //The entered number is already used    
    }else{ //Replace input placeholder and say "[number]* already taken"
        if(d == -1){
        d3.select(b)
            .attr("placeholder", "Le n°"+ bVal +" est déjà pris =/")
            .style("border-right-color" , "orange")
            .style("border-right-width", "12px")
            .style("border-style","solid")
        b.value = ""
        }
    }

    //The entered pseudo and number are available and not empty
    //Push values in [pseudolist], [slots] and [players] arrays
    if(bVal && aVal && d> -1 && c == -1 && bVal && aVal){



        //Display that pseudo is attributed inside html input
            d3.select(b)
                .attr("placeholder", "Le n°"+ bVal +" est attribué")
                .transition().style("border-right-color" , "green")
                .style("border-right-width", "12px")
                .style("border-style","solid")
            b.value = ""

        //Display that number is attributed inside html input
            d3.select(a)
                .attr("placeholder", aVal +" à été ajouté =)")
                .transition().style("border-right-color" , "green")
                .style("border-right-width", "12px")
                .style("border-style","solid") 
            a.value = ""

        //Push there value in respectives arrays
            pseudoList.push(aVal)
            slots[d] = aVal

        //Push pseudo and number value to [players] array object

        var c = d3.rgb(color(random(0,255),random(0,255),random(0,255)))

        players.push({
            "pseudo" : aVal,
            "numero" : bVal,
            "color" : c,
            "colorSel": 0,
            "click" : 0,
            "selected" : false,
            "hover":false,
        })

        //Redraw the player grid
        makeGrid()
    }
}



//Creates a form when bubble add is trigger
function bubbleForm(){
    
    this.color = color(random(0,255),random(0,255),random(0,255))

    this.display = function(){

        //Get button's position
        a = document.getElementById("buttonBubble").offsetTop 

        //Place the input next to the button
        this.bNameInp = createInput("Nommer une bulle")
        this.bNameInp.position(5,this.a)
        d3.select("#addBubble").append('input')
        .attr('type','text')
        .attr('name','textInput')
        .attr('value','Text goes here')
        //Update bubbles array and remove input container
        this.bNameInp.changed(updateBubbleName)   
    }

}

function updateBubbleName(){
   
    //Add a bubble to bubbles array
    bubbles.push({
        "name" : this.value(),
        "color" : color(random(50,200),random(50,200),random(50,200))
    })

    this.remove() //remove the name input
    bubbleNameRefresh() //refresh bubbles's names display
}

function bubbleNameRefresh(){
    
    var del = d3.selectAll("#bubbleHolder").selectAll("div").remove()
    var del2 = d3.selectAll("#sky").remove()
    var b = d3.selectAll("#bubbleHolder").append("div")
                .attr("id", "bubbleScroll")
 
    var p = b.append("svg")
    .attr("id", "nameContainerSvg")
   
    //Attach an Svg to each "g"
    var d = p.selectAll("bubblenames")
                .data(bubbles)
                .enter()
                .append("svg")
                .attr("width", 150)
                .attr("height", 20)
                .attr("y",function(d,i){
                    k=i*25 
                    return k
                })

    var f = d.append("rect")
                .attr("width", function(d,i){return d.name.length*10 +20})
                .attr("height", 20)
                .attr("stroke", function(d){return d3.rgb(d.color)})
                .attr("fill", function(d){return d3.rgb(d.color)})
                .attr("rx", "10px")
                .attr("ry", "10px")
                .on("mouseenter", nameEnter)  
    
    //Write the bubbles name inside each Svg 
    var e = d.append("text")
                .attr("x",10)
                .attr("y", 14)
                .attr("text-anchor", "start")
                .attr("alignment-baseline", "hanging")
                .attr("font-family", "sans-serif")
                .attr("font-size", "14px")
                .attr("fill", "white")
                .attr("stroke", "white")
                .attr("stroke-width", 1)
                .attr("font-weight", 900)
                .attr("padding-top", "0px")
                .attr("padding-right", "0px")
                .attr("padding-bottom", "0px")
                .attr("padding-left", "0px")
                
                .text(function(d){return d.name})

                      
}

function nameEnter(){
    console.log(this)

    d3.selectAll("#bubbleInfo").selectAll("svg").remove()

    d3.selectAll("#bubbleInfo").append("svg")
        
        .attr("width", "100%")
        .attr("height", "100%")
        .attr("position", "relative")
        .style("background-color", this.__data__.color)
        .append("text")
        .attr("x",10)
        .attr("y", 40)
        .attr("text-anchor", "start")
        .attr("alignment-baseline", "hanging")
        .attr("font-family", "sans-serif")
        .attr("font-size", "35px")
        .attr("fill", "white")
        .attr("stroke", "black")
        .attr("stroke-width", 1)
        .attr("font-weight", 900)
        .text(this.__data__.name)
}

function draw(){

    
}

