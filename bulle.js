function Bulle(x,y,r,col,zoom){

  this.x = x;
  this.y = y;
  this.zoom = zoom;
  this.col  = col;
  this.r = r;
  
  this.body = Bodies.circle(this.x,this.y,3+(this.r+this.zoom)/2);
  
  this.body.mass = 0.1*this.r;
  this.body.frictionAir = 0.02*this.r;
  this.body.density = 1;
  this.body.friction= 0.2*this.r;
  this.body.frictionStatic = 1;
  this.body.slop = -1;
  this.body.angularSpeed = 3;
  this.body.inertia = 0.01;
  this.body.restitution = 1;
  this.body.speed = 10;

  this.bubbleLocked; 
	
  var vec = createVector(this.x - windowWidth/2, this.y - windowHeight/2);

  World.add(world, this.body);


  this.move = function(){
    var pos = this.body.position;
    var vec = createVector(pos.x - windowWidth/2, pos.y - windowHeight/2);
	
    this.body.force = {x:-vec.x*0.00005,y:-vec.y*0.00005};	
  };

  this.show = function(){

    var pos = this.body.position;
    var vec = createVector(pos.x - windowWidth/2, pos.y - windowHeight/2);

    push();
    fill(this.col);
    stroke(255);
    strokeWeight(6);
    ellipse(pos.x,pos.y,this.r + this.zoom);
    pop();

  };

  console.log(this.body.bubbleLocked)
}