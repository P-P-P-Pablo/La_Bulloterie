var song
var ana
var x=0

var bubble = []
var bass 
var lowMid
var treble

function preload() {
	song = loadSound("magic.mp3")

}

function setup(){
	var cnv = createCanvas(windowWidth, windowHeight);
 	cnv.mouseClicked(togglePlay);
	fft = new p5.FFT();
	
  	song.amp(0.2);
  	amplitude = new p5.Amplitude();
  

	
}

function togglePlay() {
  if (song.isPlaying()) {
    song.pause();
  } else {
    song.loop();
  }
}


function addBubble(x,y) {

		var newBubble = new bubbleMaker(x,y)
		bubble.push(newBubble) 		
}

function draw(){
 	noStroke()
	//background(0);
	var level = amplitude.getLevel();
	var size = map(level, 0, 1, 15, windowHeight/2);
	var sample =song.getPeaks()

	if(bubble.length=0)
	addBubble(x,windowHeight/2)

	for(var i = 0; i<bubble.length; i++){
  		bubble[i].move()
  		bubble[i].show()
  	}
	
  	text('click to play/pause', 4, 10);

 	if (song.isPlaying()){
  
  	var bass = fft.getEnergy("bass")
  	var lowMid = fft.getEnergy("lowMid")
  	var treble = fft.getEnergy("treble")
  	
	
  
 	
 	
  }
  if(bb = undefined){
  	bb = 0

  	bb += map(lowMid,0,255,0,0.5)
  	addBubble(windowWidth/2,windowHeight/2)
  }
}



// fade sound if mouse is over canvas


class bubbleMaker{

	constructor(x,y) {
		this.x = x 
		this.y = y 

		this.diameter =50 + random(bass, treble)
		this.color = color(random(10,50),random(150,190),random(50,255),200)

		this.speed = p5.Vector.random2D()
		this.mass = this.diameter *0.1

		this.name
		this.players = []
	}

	show(){

		push()
		noStroke()
		fill(this.color)
		ellipse(this.x, this.y , this.diameter,this.diameter)
		pop()

	}

	move(){

	}
}